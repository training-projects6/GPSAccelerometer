//
//  GPSAccelerometerApp.swift
//  GPSAccelerometer
//
//  Created by Artem Soloviev on 27.12.2022.
//

import SwiftUI

@main
struct GPSAccelerometerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
