//
//  ContentView.swift
//  GPSAccelerometer
//
//  Created by Artem Soloviev on 27.12.2022.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject  var gps = GPSData()
    
    var body: some View {
        NavigationView{
            VStack {
                HStack {
            
                    Text("Speed")
                    Spacer()
                    Text(String (format: "%.0f",gps.speed))
                        
                }.padding()
                HStack {
                Text("0-100")
                    Spacer()
                Text( String (format: "%.1f", gps.timeCount))
            } .padding()
            HStack {
                    Text("Best 0-100")
                Spacer()
                         Text( String (format: "%.1f",gps.bestAcceleration))
                       
                } .padding()
                List{
                    ForEach(gps.accelerationArr, id: \.self) {item in
                       Text("\(item)")
                    }
                }
                Spacer()
                HStack {
                    Button(action: {
                        gps.locationManager.startUpdatingLocation()
                        UIApplication.shared.isIdleTimerDisabled = true
                    }, label: {
                        Text("start")
                            .padding()
                            .foregroundColor(.white)
                            .font(.largeTitle)
                            .padding()
                            .background(Color.blue)
                            .clipShape(Circle())
                           
                    })
                    
                    Button(action: {
                        gps.locationManager.stopUpdatingLocation()
                        UIApplication.shared.isIdleTimerDisabled = false
                        gps.reset()
                    }, label: {
                        Text("stop")
                            .padding()
                            .foregroundColor(.white)
                            .font(.largeTitle)
                            .padding()
                            .background(Color.red)
                            .clipShape(Circle())
                    })
                }.padding()
            }  .font(.title)
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
