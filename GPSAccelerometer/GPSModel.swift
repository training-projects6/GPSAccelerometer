//
//  GPSModel.swift
//  GPSAccelerometer
//
//  Created by Artem Soloviev on 27.12.2022.
//



import Foundation
import CoreLocation

class GPSData: NSObject, ObservableObject,  CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var trackingEnable = false
    var timer = Timer()
    var isActive = false
    
    @Published var authorizationStatus: CLAuthorizationStatus?
    @Published var speed: Double = 0
    @Published var timeCount: Double = 0
    @Published var accelerationArr: [Double] = []
    @Published var bestAcceleration:Double = 0
 
    override init() {
          super.init()
          locationManager.delegate = self
       }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .authorizedWhenInUse:
            authorizationStatus = .authorizedWhenInUse
            func getLocationData (){
                locationManager.startUpdatingLocation()
                locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                
            }
            break
            
        case .restricted:
            authorizationStatus = .restricted
            break
          
        case .denied:
            authorizationStatus = .denied
            break
        case .notDetermined:        // Authorization not determined yet.
            authorizationStatus = .notDetermined
            manager.requestWhenInUseAuthorization()
            break
            
        default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        speed = (locationManager.location?.speed ?? 0) * 3.6
        
        if speed > 0  && !trackingEnable {
            start()
        }
        
        if speed == 0  && trackingEnable {
            acceleration()
       
        }
        
        if speed >= 20  && trackingEnable  && isActive{
            stop()
        }

       }
   
//       func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//           print("error: \(error.localizedDescription)")
//       }
    

    func start() {
            timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
                self.timeCount += 0.1
                
            }
        trackingEnable = true
        isActive = true
        }
    
    func stop() {
        
        timer.invalidate()
        if timeCount != 0 {
            accelerationArr.append(timeCount)
        }
        bestAcceleration = accelerationArr.min() ?? 0
       isActive = false
       
    }
    
    func acceleration() {
        timer.invalidate()
        timeCount = 0
        trackingEnable = false
        isActive = true
    }

    func reset() {
        
        timer.invalidate()
        accelerationArr.removeAll()
        timeCount = 0
        trackingEnable = false
       
        
    }
}
